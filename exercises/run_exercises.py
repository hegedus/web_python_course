from importlib import import_module
from pathlib import Path
from base import Exercise
import random
from inspect import isclass


class ExercisesSuite():
    exercises_dir = Path('./exercises/')
    exercises = None

    def __init__(self):
        self.exercises = []
        # Collect all subclasses of Exercise in exercises_dir
        for module_path in self.exercises_dir.glob('*.py'):
            module = import_module('.'.join(module_path.parts[:-1] + (module_path.stem,)))
            for prop in (getattr(module, prop_name) for prop_name in dir(module) if not prop_name.startswith('_')):
                if isclass(prop) and issubclass(prop, Exercise) and (prop is not Exercise):
                    self.exercises.append(prop)


class ExercisesTextCLI():

    exercises_suite = None

    def __init__(self, exercises_suite):
        self.exercises_suite = exercises_suite

    def _print_how_to_info(self, task):
        print("""
Type your solution to file {}
and submit for solution test if you are ready.
""".format(task.solution_file_path))

    def keep_user_in_testing_loop(self, task):
        print("Would you like to start testing?")
        run_tests_choice = input("Would you like to start testing? [y/n]:")
        if run_tests_choice.lower() != 'y':
            return False
        while True:
            if task.run_test():
                print("Congrats!")
                break
            else:
                run_again_choice = input("Run test again? [y/n]:")
                if run_again_choice.lower() != 'y':
                    break

    def run_user_interaction(self):
        print("""Hi.
Try your skills!
Do you want to choose a task from list, or have one genarated randomly?
Options:
1: Select task from list
2: Generate task
'other': Quit""")
        try:
            user_input = int(input("Set your choice: "))
        except Exception:
            print("Bye.")

        if user_input == 1:
            print("This are tasks available for you")
            print("\n".join([f"{i+1}: {task.name}" for i, task in enumerate(self.exercises_suite.exercises)] + ["'other': Quit"]))
            try:
                users_task_choice = int(input("Which task would you like to try?: "))
                if users_task_choice not in range(1, len(self.exercises_suite.exercises) + 1):
                    print("You choosed number which is not in range of tasks.")
                    raise ValueError()

                task_for_user = self.exercises_suite.exercises[users_task_choice - 1]()
            except ValueError:
                print("Bye.")

        else:
            rand_task_num = random.randint(0, len(self.exercises_suite))
            print("Your task is of number: {}".format(rand_task_num+1))
            task_for_user = self.exercises_suite[rand_task_num]()

        print("Here is the description:")
        print(task_for_user)
        self._print_how_to_info(task_for_user)
        self.keep_user_in_testing_loop(task_for_user)


if __name__ == '__main__':
    exs_suite = ExercisesSuite()
    cli = ExercisesTextCLI(exs_suite)
    cli.run_user_interaction()
