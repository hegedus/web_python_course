import unittest
from pathlib import Path
from importlib import import_module, reload


class Exercise(unittest.TestCase):

    _exercise_name = None
    _solution_file_name = None
    _test_suite = None
    _sol = None
    solution_file_path = None

    def __init__(self, *args, **kwargs):
        super(Exercise, self).__init__(*args, **kwargs)
        self.solution_file_path = Path(
            './solutions').joinpath(
            self.name,
            self._solution_file_name
        )

        if not self.solution_file_path.parent.parent.exists():
            self.solution_file_path.parent.mkdir(parents=True)
            with open('__init__.py', 'w+'):
                pass
        if not self.solution_file_path.parent.exists():
            self.solution_file_path.parent.mkdir(parents=True)
            with open('__init__.py', 'w+'):
                pass

        if not self.solution_file_path.exists():
            with open(self.solution_file_path, 'w+'):
                pass

    def setUp(self):
        self._sol = getattr(
            reload(
                import_module(
                    '.' + '.'.join(
                        self.solution_file_path.parts[1:-1]
                        + (Path(self.solution_file_path.parts[-1]).stem,)
                    ),
                    'solutions')
            ),
            self._solution_func_name
        )

    def __str__(self):
        return self._exercise_name

    def print_description(self):
        raise NotImplementedError("Implement returning of Exercise description")

    def run_test(self):
        suite = unittest.TestLoader().loadTestsFromTestCase(self.__class__)
        runner = unittest.TextTestRunner()
        runner.run(suite)
