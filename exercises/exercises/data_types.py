from base import Exercise


class ReplaceOneWordInString(Exercise):
    name = 'replace_one_world_in_string'
    _solution_file_name = f'{name}.py'
    _solution_func_name = 'replace_world'

    def __str__(self):
        return "Implement function called {} which takes as first parameter string \
                and replaces there first occurence of word 'World' to 'Python'".format(self._solution_func_name)

    def test_replace_when_one_exact_world(self):
        self.assertEqual(self._sol("World"), "Python")

    def test_replace_when_one_world(self):
        self.assertEqual(self._sol("Hello World!"), "Hello Python!")
    #
    def test_replace_when_non_world(self):
        self.assertEqual(self._sol("Hello!"), "Hello!")
    #
    def test_replace_when_more_worlds(self):
        self.assertEqual(self._sol("Hello World in World!"), "Hello Python in World!")
    #
    # def test_on_empty_string(self):
    #     pass


# class ReplaceWordInString(Exercise):
#     def __str__():
#         return \
# """
#   Implement function which takes as first parameter string and replaces there first occurence of word 'World'
#   having 'message = "Hello World!"'
# """






#
