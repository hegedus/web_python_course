# Source: https://www.programiz.com/python-programming/examples/fibonacci-recursion
# Python program to display the Fibonacci sequence up to n-th term using recursive functions


def recur_fibo(n):
    """
        Recursive function to print Fibonacci sequence
    """
    if n <= 1:
        return n
    else:
        return(recur_fibo(n-1) + recur_fibo(n-2))


# Change this value for a different result
nterms = 10

# check if the number of terms is valid
if nterms <= 0:
    print("Plese enter a positive integer")
else:
    print("Fibonacci sequence:")
    for i in range(nterms):
        print(recur_fibo(i), end=' , ')

# recur_fibo(10) -> (recur_fibo(9) + recur_fibo(8))
#
# recur_fibo(1)
# recur_fibo(2)
# recur_fibo(3) == (recur_fibo(1) + recur_fibo(2))
# recur_fibo(4)) == (recur_fibo(2) + recur_1)
#
# r(4) == r(1) + r(1) + r(1)
