from collections import OrderedDict
od = OrderedDict(a=1, b=3, c=2)
od
od.items()

for k, v in od.items():
    print(k, v)


def key_func(key):
    return od[key]


sorted(od, key=lambda key: od[key])

sorted(list(od.items()))
sorted([(kv[1], kv[0]) for kv in od.items()])
sorted([(kv[1], kv[0]) for kv in od.items()])

sorted(od.items(), key=lambda kv: chr(kv[1]))

ord('a')
chr(1)


od.items





#
