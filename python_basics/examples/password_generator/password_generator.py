import string
from random import choice, randint

min_len = 8
max_len = 16


characters = string.ascii_letters + string.punctuation + string.digits

password = "".join(
    choice(characters) for x in
    range(randint(min_len, max_len)))
print(password)
