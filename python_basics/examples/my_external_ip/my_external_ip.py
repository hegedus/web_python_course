import requests
import re

url = "http://checkip.dyndns.org"

result = requests.get(url).text

theIP = re.findall(r"\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}", result)
print("your IP Address is: ",  theIP)
