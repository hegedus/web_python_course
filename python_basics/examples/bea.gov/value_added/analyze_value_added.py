import csv


# va_file_string = None
#
#
# row = va_file_string[0]
# row.split(',')
# row.split(',')[6]
# row
#
# for riatok in va_file_string:

# va_file_content = None
with open('value_added_data.csv') as f:
    # va_file_string = f.read()
    # va_file_string = f.readlines()
    va_file_content = list(csv.reader(f))

# va_file_string
va_file_content

# [1,2,3]
# [row.split(',') for row in va_file_string]
# [",".join(row) for row in va_file_content]

# va_file_content
# len(va_file_content) + 1
# sum([len(row) for row in va_file_content])

headers = va_file_content[:2]

years = []
for i in headers[0][2:]:
    if i != '':
        years.append(i)

# [i for i in headers[0][2:] if i != '']
years

quoaters = headers[1][2:]

data = va_file_content[2:]

# headers
# years
# quoaters
# data

# row = data[0]
formated_data = []
for row in data:
    # line = row[0]
    industry = row[1]
    vals_list = row[2:]
    # len_all_quoters, len_remainder = divmod(len(vals_list), 4)

    formated_vals = []
    # for i in range(len(len_all_quoters)):
    year_idx = 0
    for i, val in enumerate(vals_list):
        formated_vals.append((years[year_idx], quoaters[i], val))
        if quoaters[i] == 'IV':
            year_idx += 1

    formated_data.append((industry, formated_vals))

formated_data

# Values of year 2010
state_2010 = []
# row = formated_data[0];
for row in formated_data:
    state_2010.append(
        (
            row[0],
            list(filter(lambda t: t[0] == '2010', row[1]))
        )
    )
state_2010

# lambda t: t[0] == '2010'
#
# def is_2010(t):
#     return t[0] == '2010'

# sums by industry
sums_by_industry = []
for row in formated_data:
    # NOTE: explain list comrehension, if not yet explained
    # NOTE: explain ternary operator, if not yet explained
    summed_va = sum([int(item[2]) if item[2] != '' else 0 for item in row[1]])
    sums_by_industry.append((row[0], summed_va))

sums_by_industry

# sums by quartal
# we know the groups in advice


# more general group-by (when we don't know all the groups in advice) using default dict
from collections import defaultdict  # noqa
vas_grouped_by_quartal = defaultdict(list)
for row in formated_data:
    for data in row[1]:
        vas_grouped_by_quartal[data[1]].append(int(data[2]) if data[2] != '' else 0)
vas_grouped_by_quartal

sums_by_quartal = []
for quartal, data in vas_grouped_by_quartal.items():
    sums_by_quartal.append((quartal, sum(data)))
sums_by_quartal
