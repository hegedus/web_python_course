
# needs to be odd number
w = 25

for i in range(1, w + 2, 2):
    count_of_spaces_on_side = int((w - i) / 2)
    print((" " * count_of_spaces_on_side) + ("*" * i) + (" " * count_of_spaces_on_side))
