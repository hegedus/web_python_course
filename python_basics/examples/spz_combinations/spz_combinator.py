"""
Program to create file with all combinations of car SPZs, in format NNN(umber)-CC(haracter) Eg:'126-AE'
"""

import string
letters = string.ascii_uppercase
numbers = list(range(10))

print(letters)
print(numbers)

#
# Using just nested 'for' cycles
#

count_of_lines = 0
with open('result.txt', 'w') as f:
    for first_letter in letters:
        for second_letter in letters:
            for first_number in numbers:
                for second_number in numbers:
                    for third_number in numbers:
                        count_of_lines += 1
                        f.write('{}: {}{}{}-{}{}\n'.format(
                                count_of_lines, first_number, second_number, third_number, first_letter, second_letter))

# print results by reading the resulted file
print(count_of_lines)
with open('result.txt') as f:
    lines_count = len(f.readlines())
print(lines_count)


# count the number of variations by mathematical equation
# Those are two variation with repeating, combined with each other
# V(k,n) = n^k
# First variation: variations of three numbers from 0 to 9 = 10^3
# Second variation: variations of two characters from A to Z  = 26^2
# Combining this two variations is 'multiplication of them'
print("Count of SPZ variations should be: {}".format(
    (len(numbers)**3) * (len(letters)**2)  # In Python, the ** operator means 'power' (mocnina)
))

#
# using build-in functions
#
from itertools import product
"""
Python has a function to create cartezian product of items of the Iterables(eg. list)
"""
print(help(product))

variants_numbers = list(product(numbers, repeat=3))
variants_letters = list(product(letters, repeat=2))
spz_variants = list(product(variants_numbers, variants_letters))

print("Count of variations using itertools.product is: {}".format(
    len(spz_variants)
))
