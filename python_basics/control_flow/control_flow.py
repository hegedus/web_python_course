
#
# Branching of a control (If ... elif .. else)
#
user_input = int(input("zadaj cislo"))
if user_input == 5:
    print("zadal si cislo 5")
else:
    print("zadal si iné číslo než 5")


#
# For loop
#

list_of_words = ["one", "two", "three", "four", "five"]
for word in list_of_words:
    print(word)

# To do something 'n' times
for i in range(10):
    print(i)

#
# While loop
#

while True:
    user_input = input("do you want to quit?")
    if user_input.lower() == 'y':
        print("OK I stop")
        break
    else:
        print("Okey so I will ask you again")

counter = 0
while counter < 5:
    print('*')
    counter += 1

counter = 0
while True:
    if counter < 5:
        break
    print('*')

numbers = list(range(11))
index = -1
while True:
    index += 1

    if index == len(numbers):
        break

    if (index % 2)  == 1:
        continue

    print(numbers[index])

print("End")
