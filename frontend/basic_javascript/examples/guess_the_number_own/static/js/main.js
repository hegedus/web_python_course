let my_random_number = Math.floor(Math.random() * 100) + 1
let guesses = 1;

function handle_guess(){
  let user_input_el = document.getElementById("user_guess_input");
  let user_guess = user_input_el.value;

  if ( user_guess == "" ){
    alert("You need to guess something");
    return;
  }

  const msg_el = document.getElementById("message_for_the_user");
  let msg_text = "";

  if (user_guess == my_random_number){
    msg_text = "You Win!"
  } else if (guesses < 10){
    if (user_guess < my_random_number){
      msg_text = "Your guess is too low";
    } else {
      msg_text = "Your guess is too high";
    }
  } else {
    msg_text = "You loose"
    document.getElementById("user_guess_buton").dissabled = false;
  }
  msg_el.textContent = msg_text
  guesses++;
}

document.getElementById(
    "user_guess_buton"
).addEventListener('click', handle_guess);



//
