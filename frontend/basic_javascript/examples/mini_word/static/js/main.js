document.addEventListener('DOMContentLoaded', function(){
  // Set "text button"
  document.getElementById("text_input_btn").onclick = function(){
    document.getElementById("formated_text").innerHTML = document.getElementById("text_input").value;
  };

  // Bold button
  document.getElementById("bold_btn").onclick = function(){
    var selection = window.getSelection();
    var index_from = selection.anchorOffset;
    var index_to = selection.focusOffset;

    if ((index_from == 0) && (index_to == 0)){
      return;
    };

    // var text_el = document.getElementById("formated_text");
    var text_el = selection.anchorNode;
    var text = text_el.nodeValue;

    //swap to be from < to
    if (index_from > index_to){
      var tmp = index_from;
      index_from = index_to;
      index_to = tmp;
    }

    // format html
    var new_text = text.slice(0, index_from) +
      "<b>" + text.slice(index_from, index_to) +  "</b>" +
      text.slice(index_to, text.length)
    // set inner html
    text_el.nodeValue = new_text
  };

  // Italic button
  document.getElementById("italic_btn").onclick = function(){
    var selection = window.getSelection();
    var index_from = selection.anchorOffset;
    var index_to = selection.focusOffset;

    if ((index_from == 0) && (index_to == 0)){
      return;
    };

    var text_el = document.getElementById("formated_text");
    var text = text_el.innerHTML;

    //swap to be from < to
    if (index_from > index_to){
      var tmp = index_from;
      index_from = index_to;
      index_to = tmp;
    }

    // format html
    var new_text = text.slice(0, index_from) +
      "<i>" + text.slice(index_from, index_to) +  "</i>" +
      text.slice(index_to, text.length)
    // set inner html
    text_el.innerHTML = new_text
  };

  // Underline button
  document.getElementById("underline_btn").onclick = function(){
    var selection = window.getSelection();
    var index_from = selection.anchorOffset;
    var index_to = selection.focusOffset;

    if ((index_from == 0) && (index_to == 0)){
      return;
    };

    var text_el = document.getElementById("formated_text");
    var text = text_el.innerHTML;

    //swap to be from < to
    if (index_from > index_to){
      var tmp = index_from;
      index_from = index_to;
      index_to = tmp;
    }

    // format html
    var new_text = text.slice(0, index_from) +
      "<u>" + text.slice(index_from, index_to) +  "</u>" +
      text.slice(index_to, text.length)
    // set inner html
    text_el.innerHTML = new_text
  };


})
