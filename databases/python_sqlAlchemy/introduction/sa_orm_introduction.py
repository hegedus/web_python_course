import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text, func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Text

# Classes mapped using the Declarative system are defined in terms of a base class which maintains a catalog of classes
# and tables relative to that base - this is known as the declarative base class
Base = declarative_base()

database_url = 'mysql://{}:{}@localhost/sa_orm_tutorial'.format(os.environ['MYSQL_USER'], os.environ['MYSQL_PW'])
engine_echo = False

engine = create_engine(database_url, echo=engine_echo)


# A class using Declarative at a minimum needs a __tablename__ attribute,
# and at least one Column which is part of a primary key
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    fullname = Column(String(255))
    nickname = Column(String(255))

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (self.name, self.fullname, self.nickname)


# Create all tables which are declared in Base and does not exists in the DB yet.
Base.metadata.create_all(engine)

# Create new User instance
ed_user = User(name='ed', fullname='Ed Jones', nickname='edsnickname')


# Sesson manages persistence operations for ORM-mapped objects.
#    (https://docs.sqlalchemy.org/en/13/orm/session_api.html#sqlalchemy.orm.session.Session)
# When using ORM. The session is a way to connect to the DB and execute the statements there.
# main methods: add, add_all, expunge(oposite of add(=remove)), expunge_all, execute, query, flush, commit

# Flush vs Commit:
#    (https://stackoverflow.com/questions/4201455/sqlalchemy-whats-the-difference-between-flush-and-commit)
# The session object registers transaction operations with session.add(),
#     but doesn't yet communicate them to the database until session.flush() is called.
# session.flush() communicates a series of operations to the database (insert, update, delete).
#     The database maintains them as pending operations in a transaction. The changes aren't persisted
#         permanently to disk, or visible to other transactions until the database receives a COMMIT for
#         the current transaction (which is what session.commit() does).
# session.commit() commits (persists) those changes to the database.
# flush() is always called as part of a call to commit() (1).

# Create Session class binded with engine
Session = sessionmaker(bind=engine)
# Create one new inscance of a Session
session = Session()

# add instance of the user
session.add(ed_user)

session.query(User).filter_by(name='ed').first()

# add mulpitple users at once
session.add_all([
    User(name='wendy', fullname='Wendy Williams', nickname='windy'),
    User(name='mary', fullname='Mary Contrary', nickname='mary'),
    User(name='fred', fullname='Fred Flintstone', nickname='freddy')]
)
session.query(User).all()

# flush the changes (communicate them to the DB) and send commit command to the DB to persist the changes.
session.commit()


session.query(User).order_by(User.id).all()


# Any time multiple class entities or column-based entities are expressed as arguments to the query() function,
#    the return result is expressed as tuple
session.query(User.name, User.fullname).all()


session.query(User.name).filter(User.fullname == 'Ed Jones').all()

session.query(User).filter(User.name == 'ed').filter(User.fullname == 'Ed Jones').all()

session.query(User).filter(User.name.in_(['ed', 'wendy', 'jack'])).all()


# using textual sql
session.query(User).filter(text("id<224")).order_by(text("id")).all()

session.query(User).filter(User.name.like('%ed')).count()

# aggregations
session.query(func.count(User.name), User.name).group_by(User.name).all()


# Relationsips between tables

# One to Many
class Address(Base):
    __tablename__ = 'addresses'
    id = Column(Integer, primary_key=True)
    email_address = Column(String(255), nullable=False)
    # this defines 1:m relationsip
    user_id = Column(Integer, ForeignKey('users.id'))
    # The 'user' field will contain an instance of the User
    # back_populates means which field should be populated in the corresponding table object(the Session will manage it)
    #   (https://docs.sqlalchemy.org/en/13/orm/relationship_api.html#sqlalchemy.orm.relationship)
    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return "<Address(email_address='%s')>" % self.email_address


# add coresponding field also tu the other table
User.addresses = relationship("Address", order_by=Address.id, back_populates="user")

Base.metadata.create_all(engine)

jack = User(name='jack', fullname='Jack Bean', nickname='gjffdd')
jack.addresses
jack.addresses = [
    Address(email_address='jack@google.com'),
    Address(email_address='j25@yahoo.com')
]
jack.addresses[1]

jack.addresses[1].user

session.add(jack)

session.commit()

jack = session.query(User).filter_by(name='jack').one()
jack
jack.addresses

# Join tables
session.query(User, Address).filter(User.id == Address.user_id).filter(Address.email_address == 'jack@google.com').all()

session.query(User).join(Address).filter(Address.email_address == 'jack@google.com').all()

# M to N relationship

# We need to explicitly define association table
post_keywords = Table(
    'post_keywords',
    Base.metadata,
    Column('post_id', ForeignKey('posts.id'), primary_key=True),
    Column('keyword_id', ForeignKey('keywords.id'), primary_key=True)
)


class BlogPost(Base):
    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    headline = Column(String(255), nullable=False)
    body = Column(Text)

    # many to many BlogPost<->Keyword
    keywords = relationship('Keyword',
                            secondary=post_keywords,
                            back_populates='posts')

    def __init__(self, headline, body, author):
        self.author = author
        self.headline = headline
        self.body = body

    def __repr__(self):
        return "BlogPost(%r, %r, %r)" % (self.headline, self.body, self.author)


class Keyword(Base):
    __tablename__ = 'keywords'

    id = Column(Integer, primary_key=True)
    keyword = Column(String(50), nullable=False, unique=True)
    posts = relationship('BlogPost',
                         secondary=post_keywords,
                         back_populates='keywords')

    def __init__(self, keyword):
        self.keyword = keyword


BlogPost.author = relationship(User, back_populates="posts")
User.posts = relationship(BlogPost, back_populates="author", lazy="dynamic")
Base.metadata.create_all(engine)

wendy = session.query(User).filter_by(name='wendy').one()
post = BlogPost("Wendy's Blog Post", "This is a test", wendy)
session.add(post)

post.keywords.append(Keyword('wendy'))
post.keywords.append(Keyword('firstpost'))

# (https://docs.sqlalchemy.org/en/13/orm/internals.html#sqlalchemy.orm.properties.RelationshipProperty.Comparator.any)
session.query(BlogPost).filter(BlogPost.keywords.any(keyword='firstpost')).all()

session.query(BlogPost).filter(BlogPost.author == wendy).filter(BlogPost.keywords.any(keyword='firstpost')).all()

wendy.posts.filter(BlogPost.keywords.any(keyword='firstpost')).all()


#
