import os
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.sql import text, select
from sqlalchemy.sql import and_, or_  # , not_
from sqlalchemy import table, column
from sqlalchemy import desc
from sqlalchemy import distinct, func

"""
This is a basis introduction to SQL Alchemy.
We will follow the official documentation of https://docs.sqlalchemy.org/en/13/

At the latest stage, the developer who "knows" the SQLAlchemy(or most of other libraries)
is the one who is handy with it's official documentation.

That's why I see our task, not to avoid the documentation but to make it more easy and accessible
for people starting with multiple concepts at once: The Python, Databases, SQLAlchemy
(Because documentattion of SQLAlchemy is mostly wrote on level of developers already well aware of the DBs concepts.)

The diagram showing basic design of SQL Alchemy can be found here:
https://docs.sqlalchemy.org/en/13/intro.html

There is the Engine which we can ask for a Connection.

The Connection object we can ask to execute our SQLStatement.

The ConnectionPool is the intermadiate object managing the connections. Normally we don't need to work directly with it.
The Dialec is based on the type of the DB (Eg. MySQL, PostgreSQL, SQLLite) and it provides support for them

There is module of "SQL Expression Language" which supports different ways of defining the sql query in the python code.

Schema/Types is for keeping and managing infrormation about database tables, columns and types.

The ORM module is for creating more high-level classes with more implicit features
then the functionality of SQL Alchemy's Core.


Generaly, the SQL Alchemy provides a lot of ways to do the same thing.
It's because it is a framework facing a lot of use cases, trying to support all of them.
(In comparison with Django-ORM which has a narrower scope focused more on high-level features.)

"""

#
# Simplifying and explanations of https://docs.sqlalchemy.org/en/13/core/tutorial.html
#

#
# You need the mysql installed on localhost and created the database called sa_tutorial
# to create the db connect to DB using text client and run 'create database sa_tutorial;'


# Either set the environment variables MYSQL_USER and MYSQL_PW or place the values directly to string
#   It's adviced to use environmnet variables
#   for linux: export MYSQL_USER=<user_name>; export MYSQL_PW=<password>  (replace <...> with actual value)
#   for windows: set MYSQL_USER=<user_name>; set MYSQL_PW=<password>
# If you want to have the env variables accessible in 'Atom - Hydrogen' set the variables in the cmd and then
# run the Atom from the same cmd.
#
# The documentation about database urls:
#    https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
database_url = 'mysql://{}:{}@localhost/sa_tutorial'.format(os.environ['MYSQL_USER'], os.environ['MYSQL_PW'])
# If True it enables debug messages on standard output
engine_echo = False

# This will create the engine object. The dialect which will be used is recognized by database_url
# Note that this is just the setting up and we don't create call to DB yet.
#   So if the connection string is incorrect you will not have the information about it by this call.
#   It will be found out when you will try to do some actual call to the DB.
engine = create_engine(database_url, echo=engine_echo)

# Medatada stores the information about database, tables and columns
# SQL Alchemy uses this information to produce correct SQL statement when parsing the
#    statements wrote in python (using SA SQL Expression language)
metadata = MetaData(bind=engine)


# Definition of the Tables
# There are many ways of defining the tables.
# This is one example of definition on level somewhere between ORM and "low level table definition"
users = Table('users', metadata,
    Column('id', Integer, primary_key=True),  # noqa  # The noqa is special keyword for Atom's linter package, it's to suppress the warning (SQL Alchemy sometimes produces some)
    Column('name', String(100)),
    Column('fullname', String(255)),
)

addresses = Table('addresses', metadata,
    Column('id', Integer, primary_key=True),  # noqa
    Column('user_id', None, ForeignKey('users.id')),
    Column('email_address', String(255), nullable=False)
)

# Note that not just users and addresses have the refference to metadata object as we pass it to the constructor.
# The Table registers itself in the metadata object durring initialization. That's why also metadata object contains
#   the refference to all tables create with this metadata.
# This is the call to create all tables registered in metadata.
# You need to do it just first time. Next time skip this call.
metadata.create_all(engine)
# Now in the Mysql client you can run commands like:
# use sa_tutorial;
# show tables;
# describe addresses;
# And you should see created tables and their attributes.


#
# # Inserting data
# You need to do it just first time. Next time skip the insertion calls.
#

# The connection object represents one created connection with the Database
# We can call engine.connect() multiple times to provide to us multiple connection if needed.
conn = engine.connect()

conn.execute(
    # the keywords match the name's of the columns and the values are the values to insert
    # columnt 'id' is autoincrement
    users.insert().values(name='jack', fullname='Jack Jones')
)
# A Little bit different way
# Notice that now we put values to parameter of 'execute' method of connection object.
#    Previously we put them to method of the users object.
conn.execute(users.insert(), id=2, name='wendy', fullname='Wendy Williams')

# to insert multiple data in one query:
conn.execute(
    addresses.insert(),
    [
        {'user_id': 1, 'email_address': 'jack@yahoo.com'},
        {'user_id': 1, 'email_address': 'jack@msn.com'},
        {'user_id': 2, 'email_address': 'www@www.org'},
        {'user_id': 2, 'email_address': 'wendy@aol.com'},
    ]
)

#
# # Printing the queries
# You can print the created queries by print function.
# Note that the queries will not be executed unless you put them as a parameter of conn.execute() method
# You can do it on any kind of query (insert, update, delete, select)
print(users.insert().values(name='jack', fullname='Jack Jones'))
# If the query contains values, SQL alchemy will not put them directly to the SQL.
# It's called "prepared statements" and one of the big advantage of it is the protection agains SQL Injection Attacks.
# If you want to see the statement with the values too, use:
print(users.insert().values(name='jack', fullname='Jack Jones').compile(compile_kwargs={"literal_binds": True}))


#
# # Selecting the data
#

# For Selecting the data there is function 'select'
# docs.sqlalchemy.org/en/13/core/selectable.html#sqlalchemy.sql.expression.select
# select if flexible general function able to consume a lot of different ways of writing down the queries
#   As we said previously it's one of the goals of the SQL Alchemy to be versatile.

# It produces the iterable of result rows
result = conn.execute(
    select([users])
)

for row in result:
    print(row)

# Cartesian product
print('\n'.join(map(str, list(
    conn.execute(
        select([users, addresses])
    )
))))

# Example of more advanced query
# Table.c is the object for accesing the columns ('c' as 'c'olumn)
#    We can call operator on the columns like u.c.firstname == 'one', and the result of this call will not be
#    the boolean value as in pure python. The SQLAlchemy.Column overwrites the operators and provides own return objects
#    this objects are then used to generate the SQL Query.
#    (You can try to run eg. print(users.c.id == addresses.c.user_id) and see it is the object of the class
#        sqlalchemy.sql.elements.BinaryExpression )

s = select(
    [(users.c.fullname + ", " + addresses.c.email_address).label('title')]
).where(
    and_(
        users.c.id == addresses.c.user_id,
        users.c.name.between('m', 'z'),
        or_(
            addresses.c.email_address.like('%@aol.com'),
            addresses.c.email_address.like('%@msn.com')
        )
    )
)
conn.execute(s).fetchall()

# Using textual SQL
list(conn.execute(text('select * from users')))

list(conn.execute(text('select * from users').columns(id=Integer, name=String, fullname=String)))

list(conn.execute(text(
    "SELECT users.id, addresses.id, users.id, "
    "users.name, addresses.email_address AS email "
    "FROM users JOIN addresses ON users.id=addresses.user_id "
    "WHERE users.id = 1"
).columns(
    users.c.id,
    addresses.c.id,
    addresses.c.user_id,
    users.c.name,
    addresses.c.email_address
)))

#
# functions table, column.
#   These produces the TableClause and ColumnClause which are lightweight versionf of Table and Column
#
users_table = table(
    "users",
    column("id"),
    column("name"),
    column("fullname"),
)
list(conn.execute(select([users_table.c.fullname]).where(users_table.c.name == 'wendy')))

# Order BY
list(conn.execute(select([users_table]).order_by(desc(users_table.c.name))))

list(conn.execute(select([func.count(distinct(users_table.c.name))])))

# Group By
conn.execute(select([
    addresses.c.user_id,
    func.count(addresses.c.id).label('num_addresses')
    ]
).group_by("user_id").order_by("user_id", "num_addresses")).fetchall()



#
# Subqueries
#

s1=select([users]).where(
and_(
    users.c.id == addresses.c.user_id,
    addresses.c.email_address == 'jack@msn.com',
)).alias()
print(s1)

print(select([s1.c.name]).where(s1.c.id == 1))
conn.execute(select([s1.c.name]).where(s1.c.id == 1)).fetchall()
conn.execute(select([column('name')]).where(s1.c.id == 1)).fetchall()
conn.execute(select([column('name')]).select_from(s1)).fetchall()
conn.execute(select([s1.c.name])).fetchall()




#
