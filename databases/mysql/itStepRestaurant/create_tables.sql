use itStepRestaurant;

CREATE TABLE IF NOT EXISTS Meals (
    id INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    price INT NOT NULL,
    cookingTime INT,

    PRIMARY KEY (id)
);

-- Created by MySQL Workbench
CREATE TABLE `itStepRestaurant`.`Orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created` DATETIME NOT NULL,
  `tableNumber` INT NULL,
  `countOfPeople` INT NULL,
  `waiterName` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

CREATE TABLE `itStepRestaurant`.`OrdersMeals` (
  `order_id` INT NOT NULL,
  `meal_id` INT NOT NULL,
  -- PRIMARY KEY (`order_id`, `meal_id`),
  INDEX `fk_OrdersMeals_2_idx` (`meal_id` ASC),
  CONSTRAINT `fk_OrdersMeals_1`
    FOREIGN KEY (`order_id`)
    REFERENCES `itStepRestaurant`.`Orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OrdersMeals_2`
    FOREIGN KEY (`meal_id`)
    REFERENCES `itStepRestaurant`.`Meals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
