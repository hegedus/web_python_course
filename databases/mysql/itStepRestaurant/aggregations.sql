
-- join tables Orders, OrdersMeals, Meals to one table, prepared for aggregation

create temporary table AggPreparedData as (
    select o.id as orderId, o.created, o.tableNumber, o.CountOfPeople,
          o.waiterName, Meals.name as mealName, Meals.price,
          Meals.cookingTime
    from Orders o
    join OrdersMeals om
    on  o.id=om.order_id
    join Meals on Meals.id=om.meal_id
);

-- Calculate price of each order
-- Alternative 1
select
	orderId,
	max(created) as created,
  max(tableNumber) as tableNumber,
  max(CountOfPeople) as CountOfPeople,
  max(waiterName) as waiterName,
  group_concat(" ", mealName) as MealsNames,
  sum(price) as Price,
  max(cookingTime) as CookingTime
from AggPreparedData
group by orderId
;

-- Alternative 2
select
	orderId,
	created,
	tableNumber,
	CountOfPeople,
	waiterName,
  group_concat(" ", mealName) as MealsNames,
  sum(price) as Price,
  sum(cookingTime) as CookingTime
from AggPreparedData
group by orderId, created, tableNumber CountOfPeople, waiterName
;

-- How much does the waiters earn
select waiterName, sum(price) from AggPreparedData group by waiterName;

-- How much does the waiters and tables earn
select max(waiterName), tableNumber, sum(price) from AggPreparedData group by tableNumber;
select waiterName, tableNumber, sum(price) from AggPreparedData group by waiterName, tableNumber;


select * from AggPreparedData;
select
	orderId,
	created,
	tableNumber,
	CountOfPeople,
	waiterName,
  group_concat(" ", mealName) as MealsNames,
  sum(price) as Price,
  sum(cookingTime) as CookingTime
from AggPreparedData
where cookingTime>5
group by orderId, created, tableNumber, CountOfPeople, waiterName

select * from AggPreparedData;
select
	orderId,
	created,
	tableNumber,
	CountOfPeople,
	waiterName,
  group_concat(" ", mealName) as MealsNames,
  sum(price) as Price,
  sum(cookingTime) as CookingTime
from AggPreparedData
group by orderId, created, tableNumber, CountOfPeople, waiterName
having cookingTime>25;


select * from AggPreparedData;


select
	orderId,
	created,
	tableNumber,
	CountOfPeople,
	waiterName,
  group_concat(" ", mealName) as MealsNames,
  sum(price) as Price,
  sum(cookingTime) as CookingTime
from AggPreparedData
where cookingTime>5
group by orderId, created, tableNumber, CountOfPeople, waiterName
having cookingTime>25;






--
