use itStepRestaurant;

INSERT INTO Meals
    (name, price, cookingTime)
VALUES
    ('Pizza Quattro Formaggy',	250,	15),
    ('Pizza Vegetarianna',	250,	15),
    ('Greek Salad',	200,	5),
    ('Medetarian Salad',	200,	5),
    ('Pasta Crema di Fungi',	250,	null),
    ('Pasta Arabica',	200,	10);


INSERT INTO `itStepRestaurant`.`Orders` (`created`, `tableNumber`, `countOfPeople`, `waiterName`) VALUES ('2019-09-07 10:00', '1', '3', 'Philippe');
INSERT INTO `itStepRestaurant`.`Orders` (`created`, `tableNumber`, `countOfPeople`, `waiterName`) VALUES ('2019-09-07 11:00:00', '4', '2', 'Philippe');
INSERT INTO `itStepRestaurant`.`Orders` (`created`, `tableNumber`, `countOfPeople`, `waiterName`) VALUES ('2019-09-08 12:00:00', '2', '4', 'Samantha');


INSERT INTO OrdersMeals
VALUES
  (1, 1),
  (1, 4),
  (1, 6),
  (2, 3),
  (2, 2),
  (2, 4);


INSERT INTO OrdersMeals
VALUES
  (3, 1),
  (3, 3),
  (3, 2),
  (3, 6);






--
