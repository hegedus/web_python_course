from guess_number_oop import SecretKeeper
import unittest


class TestSecretKeeper(unittest.TestCase):
    def setUp(self):
        self.secret_keeper = SecretKeeper()

    def test_gt(self):
        self.secret_keeper._secret = 50
        # self.assertGreater(self.secret_keeper, 51)
        self.assertGreater(self.secret_keeper, 49)
        self.assertGreater(self.secret_keeper, 0)
        self.assertGreater(self.secret_keeper, -1)
        self.assertGreater(self.secret_keeper, -50)

        self.secret_keeper._secret = -50
        self.assertGreater(self.secret_keeper, -51)
        self.assertGreater(self.secret_keeper, -100)

    def test_ge(self):
        self.secret_keeper._secret = 50
        self.assertGreater(self.secret_keeper, 49)
        self.assertGreater(self.secret_keeper, 0)
        self.assertGreater(self.secret_keeper, -1)
        self.assertGreater(self.secret_keeper, -50)

        self.assertEqual(self.secret_keeper, 50)

        self.secret_keeper._secret = -50
        self.assertGreater(self.secret_keeper, -51)
        self.assertGreater(self.secret_keeper, -100)

    def test_lt(self):
        self.secret_keeper._secret = 50
        self.assertLess(self.secret_keeper, 51)
        self.assertLess(self.secret_keeper, 100)

        self.secret_keeper._secret = -50
        self.assertLess(self.secret_keeper, -1)
        self.assertLess(self.secret_keeper, 0)
        self.assertLess(self.secret_keeper, 50)

    def test_le(self):
        self.secret_keeper._secret = 50
        self.assertLess(self.secret_keeper, 51)
        self.assertLess(self.secret_keeper, 100)

        self.assertEqual(self.secret_keeper, 50)

        self.secret_keeper._secret = -50
        self.assertLess(self.secret_keeper, -1)
        self.assertLess(self.secret_keeper, 0)
        self.assertLess(self.secret_keeper, 50)

    def test_eq(self):
        self.secret_keeper._secret = 50
        self.assertEqual(self.secret_keeper, 50)


suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSecretKeeper)
unittest.TextTestRunner().run(suite)













#
