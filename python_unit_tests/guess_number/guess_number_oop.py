import random


class SecretKeeper():

    _secret = None

    def __init__(self):
        self._secret = random.randint(1, 99)

    def __lt__(self, num):
        return self._secret < num

    def __le__(self, num):
        return self._secret <= num

    def __gt__(self, num):
        return self._secret > num

    def __ge__(self, num):
        return self._secret > num

    def __eq__(self, num):
        return self._secret == num


class Game:
    _secret_keeper = None
    _max_tries = None
    _tried = 0

    def __init__(self, secret_keeper, max_tries=10):
        self._max_tries = max_tries
        self._secret_keeper = secret_keeper

    def play(self):
        guess = int(input("Enter an integer from 1 to 99: "))
        while (guess != 'q') and (self._tried < self._max_tries):
            if (guess < self._secret_keeper):
                print("guess is low")
                inp = input("Enter an integer from 1 to 99: ")
                # if inp == 'q':
                #     break
                guess = int(inp)
            elif guess > self._secret_keeper:
                print("guess is high")
                inp = input("Enter an integer from 1 to 99: ")
                # if inp == 'q':
                #     break
                guess = int(inp)
            else:
                print("you guessed it!")
                break

            self._tried += 1

        print("Game Over")


if __name__ == '__main__':
    game = Game(SecretKeeper(), 10)
    game.play()
