
from itertools import product
list(product('abcdefgh', range(1, 9)))


class Position():
    x = None
    y = None

    def __init__(self, x, y):
        if not ('a' <= x <= 'h'):
            raise ValueError("X needs to be str of a-h. Paased: {}".format(x))
        if not (1 <= y <= 8):
            raise ValueError("Y needs to be str of a-h. Paased: {}".format(y))
        self.x = x
        self.y = y

    def __str__(self):
        return "Position({}, {})".format(self.x, self.y)


class Figure():
    color = None
    position = None

    def __init__(self, color, position):
        if not isinstance(position, Position):
            raise ValueError("position needs to be instanco of Position. Passed: {}".format(position))
        self.color = color
        self.position = position

    def move(self, to_position):
        raise NotImplemented()


class Pawn(Figure):
    def move(self, to_position):
        print("Moving to position: {}".format(to_position))
        pass


p = Pawn('white', Position('b', 2))
p.move(Position('b', 4))

class CheeseBoard():
    board = [
        []i
    ]
    figures = [
    ('a', 1'): Rook(position)
    ]
    def print()
