
from shapes_oop import Shape, Rectangle, Triangle, XShape


class ShapesDrawer():
    shapes = {}

    def __init__(self, **kwargs):
        for shape_name, shape_obj in kwargs.items():
            if not issubclass(type(shape_obj), Shape):
                raise Exception("Shape object must by instance of shapes_oop.Shape not {}".format(type(shape_obj)))
            self.shapes[shape_name] = shape_obj

    def register_shapes(self, **kwargs):
        pass

    def unregister_shapes(self, *args):
        pass

    def draw(self, shape_name):
        self.shapes[shape_name].draw()


class DrawingCLI():
    drawer = None

    menu_mapping = {
        "1": 'rectangle',
        "2": 'triangle',
        "3": 'xshape',
    }

    def __init__(self, drawer):
        self.drawer = drawer

    def print_help(self):
        print("This is program to draw shapes.")

    def print_menu(self):
        print("Choose one of the choses or press 'q' to quit")
        print("1. Draw Rectangle")
        print("2. Draw Triange")
        print("3. Draw X")

    def listen_menu(self):
        # self.print_menu()
        user_input = input()
        return user_input

    def handle_choose(self, user_input):
        if user_input == 'q':
            return False
        if user_input not in self.menu_mapping:
            raise ValueError("Menu option {} not found.".format(user_input))

        self.drawer.draw(self.menu_mapping[user_input])

        return True

    def keep_user_in_loop(self):
        while True:
            self.print_menu()
            if not self.handle_choose(self.listen_menu()):
                break


if __name__ == '__main__':
    drawer = ShapesDrawer(rectangle=Rectangle(), triangle=Triangle(), xshape=XShape())
    drawing_cli = DrawingCLI(drawer)
    drawing_cli.keep_user_in_loop()












#
