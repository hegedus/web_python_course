

class Shape:
    def draw(self):
        print("I'm just a generall class")


class Rectangle(Shape):
    a = 10
    b = 7

    def draw(self):
        print("*"*self.a)
        for i in range(self.b-2):
            print("*" + (" " * (self.a-2)) + "*")
        print("*"*self.a)

    def print_yourself(self):
        print("Hi, I'm rectangle.")


class Triangle(Shape):
    def draw(self):
        # Triangle
        # needs to be odd number
        w = 25

        for i in range(1, w + 2, 2):
            count_of_spaces_on_side = int((w - i) / 2)
            print((" " * count_of_spaces_on_side) + ("*" * i) + (" " * count_of_spaces_on_side))

    def print_yourself(self):
        print("Hi, I'm Triangle.")


class XShape(Shape):
    def draw(self):
        # needs to be odd number
        a = 11
        for i in range(int(a/2)):
            # print(i)
            # "some"[2] = '*'
            print((" " * i) + "*" + (" " * ((a - (2*i)) - 2)) + "*" + (" " * i))

        print((" " * int(a/2)) + "*" + (" " * int(a/2)))

        for i in range(int(a/2)-1, -1, -1):
            # print(i)
            print((" " * i) + "*" + (" " * ((a - (2*i)) - 2)) + "*" + (" " * i))

    def print_yourself(self):
        print("Hi, I'm XShape.")


rectangle = Rectangle()
triangle = Triangle()
x = XShape()

rectangle.draw()
triangle.draw()
x.draw()

#
