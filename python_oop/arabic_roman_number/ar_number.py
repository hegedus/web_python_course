class Number:

    _num = None

    def __init__(self, num):
        self._num = self._to_int(num) if isinstance(num, str) else num

    def as_roman(self):
        return self._to_roman(self._num)

    def as_int(self):
        return self._num

    def _to_roman(self, num):
        val = [
            1000, 900, 500, 400,
            100, 90, 50, 40,
            10, 9, 5, 4,
            1
            ]
        syb = [
            "M", "CM", "D", "CD",
            "C", "XC", "L", "XL",
            "X", "IX", "V", "IV",
            "I"
            ]
        roman_num = ''
        i = 0
        while num > 0:
            for _ in range(num // val[i]):
                roman_num += syb[i]
                num -= val[i]
            i += 1
        return roman_num

    def _to_int(self, s):
        rom_val = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        int_val = 0
        for i in range(len(s)):
            if i > 0 and rom_val[s[i]] > rom_val[s[i - 1]]:
                int_val += rom_val[s[i]] - (2 * rom_val[s[i - 1]])
            else:
                int_val += rom_val[s[i]]
        return int_val

        # This is how it can be done other way
        # int_vals = [rom_val[i] for i in s]
        #
        # len_int_vals = len(int_vals)
        #
        # substaction_map = [
        #     True if i < (len_int_vals - 1) and val < int_vals[i+1] else False for i, val in enumerate(int_vals)
        # ]
        #
        # sum_to_add = sum([val for i, val in enumerate(int_vals) if not substaction_map[i]])
        # sum_to_substract = sum([val for i, val in enumerate(int_vals) if substaction_map[i]])
        # res = sum_to_add - sum_to_substract
        # return res


if __name__ == '__main__':
    print(Number(10).as_roman())
    print(Number(162).as_roman())
    print(Number(162).as_int())
    print(Number('X').as_int())
    print(Number('CLXII').as_int())
