from itertools import product
from random import shuffle
from pprint import pprint


class Deck:
    _cards = []

    def __init__(self, card_types, card_values):
        self._cards = list(product(card_types, card_values))

    def show_cards(self):
        pprint(self._cards)

    def deal(self):
        top_card = self._cards[0]
        del self._cards[0]
        return top_card

    def shuffle(self):
        shuffle(self._cards)

    def __len__(self):
        return len(self._cards)


card_types = ('Hearts', 'Diamonds', 'Clubs', 'Spades')
card_values = ('A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K')

our_deck = Deck(card_types, card_values)
print("Deck contains {} cards.".format(len(our_deck)))

our_deck.shuffle()

print(our_deck.deal())

our_deck.show_cards()


#
# Task: Make Deck iterable
#
# for card in our_deck:
#     print("card: ".format(card))

# """
# for =
# """
# our_iterator = our_deck.__iter__()
# card = our_iterator.next():
# while card:
#     card = our_iterator.next():
#
#     <...our code of a cycle...>
#




#
