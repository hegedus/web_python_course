from .common import Shape

class Rectangle(Shape):
    a = 10
    b = 7

    def draw(self):
        print("*"*self.a)
        for i in range(self.b-2):
            print("*" + (" " * (self.a-2)) + "*")
        print("*"*self.a)

    def print_yourself(self):
        print("Hi, I'm rectangle.")
