from .common import Shape


class Triangle(Shape):
    def draw(self):
        # Triangle
        # needs to be odd number
        w = 25

        for i in range(1, w + 2, 2):
            count_of_spaces_on_side = int((w - i) / 2)
            print((" " * count_of_spaces_on_side) + ("*" * i) + (" " * count_of_spaces_on_side))

    def print_yourself(self):
        print("Hi, I'm Triangle.")
