from shapes.common import Shape


class ConsoleShapesDrawer():
    shapes = {}

    def __init__(self, **kwargs):
        for shape_name, shape_obj in kwargs.items():
            if not issubclass(type(shape_obj), Shape):
                raise Exception("Shape object must by instance of shapes_oop.Shape not {}".format(type(shape_obj)))
            self.shapes[shape_name] = shape_obj

    def register_shapes(self, **kwargs):
        pass

    def unregister_shapes(self, *args):
        pass

    def get_shapes(self):
        return sorted(self.shapes.keys())

    def draw(self, shape_name):
        self.shapes[shape_name].draw()
