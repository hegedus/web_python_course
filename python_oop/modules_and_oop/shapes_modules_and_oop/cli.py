
class DrawingCLI():
    drawer = None
    menu_mapping = None

    def __init__(self, drawer, menu_mapping):
        self.drawer = drawer
        self.menu_mapping = menu_mapping

    def print_help(self):
        print("This is program to draw shapes.")

    def print_menu(self):
        print("Choose one of the choses or press 'q' to quit")
        for i, shape_name in self.menu_mapping.items():
            print("{}. Draw {}".format(i, shape_name.capitalize()))

    def listen_menu(self):
        # self.print_menu()
        user_input = input()
        return user_input

    def handle_choose(self, user_input):
        if user_input == 'q':
            return False
        if user_input not in self.menu_mapping:
            raise ValueError("Menu option {} not found.".format(user_input))

        self.drawer.draw(self.menu_mapping[user_input])

        return True

    def keep_user_in_loop(self):
        while True:
            self.print_menu()
            if not self.handle_choose(self.listen_menu()):
                break
