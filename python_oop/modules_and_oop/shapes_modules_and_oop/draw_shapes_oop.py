from shapes.rectangles import Rectangle
from shapes.triangles import Triangle
from shapes.other_shapes import XShape
from drawer import ConsoleShapesDrawer
from cli import DrawingCLI


if __name__ == '__main__':
    # Initialize drawer object
    drawer = ConsoleShapesDrawer(rectangle=Rectangle(), triangle=Triangle(), xshape=XShape())

    # Create menu mapping from the available drawer shapes
    drawer_shapes = drawer.get_shapes()
    menu_mapping = {str(i): shape_name for i, shape_name in enumerate(drawer_shapes, 1)}

    # Initialize and run DrawingCLI
    drawing_cli = DrawingCLI(drawer, menu_mapping)
    drawing_cli.keep_user_in_loop()











#
