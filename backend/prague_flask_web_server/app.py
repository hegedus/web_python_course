from flask import Flask, render_template
app = Flask(__name__)


@app.route('/')
@app.route('/index.html')
def index():
    return render_template('index.html')


@app.route('/basic_info.html')
def basic_info():
    return render_template('basic_info.html')


@app.route('/photos.html')
def photos():
    return render_template('photos.html')


@app.route('/theatres.html')
def theatres():
    return render_template('theatres.html')


app.run(host="localhost", port="5000", debug=True)
