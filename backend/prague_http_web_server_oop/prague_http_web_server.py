import os
import re
import mimetypes
from http.server import HTTPServer, BaseHTTPRequestHandler


class PragueRequestHandler(BaseHTTPRequestHandler):

    # regex expression to match supported url patterns
    # url_path == ('.' + file_path) eg: page /index.html will be provided from ./index.html
    supported_pages = (r'^/.*\.html$', r'^/static/.*')
    _supported_pages_rgx = re.compile('|'.join(['({})'.format(r) for r in supported_pages]))

    def do_GET(self):
        print("Req accepted")

        if self.path == '/':
            with open('index.html', 'r') as index_f:
                headers = "HTTP/1.1 200 OK\nContent-Type: text/html charset=utf-8"
                body = index_f.read()
                html_to_send = '{}\n\n{}'.format(headers, body).encode()
                self.wfile.write(html_to_send)

        elif self._supported_pages_rgx.match(self.path):
            file_path ='.{}'.format(self.path)

            mime_type = mimetypes.guess_type(file_path)[0]
            if not mime_type:
                print("Warning: Could not found a mime_type of a file: {]".format(file_path))
                self.wfile.write("HTTP/1.1 500 Server Error")
                return

            with open(file_path, 'rb') as f:
                headers = (
                    "HTTP/1.1 200 OK",
                    "Content-Type: {} charset=utf-8".format(mime_type),
                    "Content-Length: {}".format(os.stat(file_path).st_size)
                )
                content = f.read()
                http_to_send = '{}\n\n'.format('\n'.join(headers)).encode() + content
                self.wfile.write(http_to_send)

        else:
            print("NOT FOUND: {}".format(self.path))
            self.wfile.write("HTTP/1.1 404 NOT FOUND\n\n".encode())



with HTTPServer(('', 8000), PragueRequestHandler) as prague_http_server:
    prague_http_server.serve_forever()


#
