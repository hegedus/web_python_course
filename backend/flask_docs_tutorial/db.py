import os
import click
from flask import current_app, g
from flask.cli import with_appcontext
from pathlib import Path


class FileDatabase():

    root_path = None

    def __init__(self, root_path):
        self.root_path = Path(root_path)

    def exists(self, file_path):
        pass

    def get_all_data(self, obj_name):
        obj_path = self.root_path.joinpath(obj_name)
        if not obj_path.exists():
            return []

        all_data = []
        for file_path in obj_path.iterdir():
            file_content = None
            with file_path.open() as f:
                file_content = f.readlines()
            if not file_content:
                continue

            obj_data_dict = {k: v for k, v in [
                    (
                        line[:line.index('=')],
                        line[line.index('=')+1:],
                    )
                    for line in file_content
                ]
            }
            obj_data_dict['id'] = file_path.name
            all_data.append(obj_data_dict)

        return all_data

    def get_data(self, obj_name, id):
        file_path = self.root_path.joinpath(obj_name).joinpath(str(id))
        if file_path.exists():
            obj_data_dict = None
            with file_path.open() as f:
                obj_data_dict = {k: v for k, v in [
                        (
                            line[:line.index('=')],
                            line[line.index('=')+1:]
                        )
                        for line in f.readlines()
                    ]
                }
            obj_data_dict['id'] = str(id)
            return obj_data_dict
        else:
            return None

    def _get_next_id(self, obj_name):
        obj_path = self.root_path.joinpath(obj_name)
        if not obj_path.exists():
            return None

        obj_items = [p.name for p in obj_path.iterdir()]
        if not obj_items:
            return 1

        return int(sorted(obj_items)[-1]) + 1

    def put_data(self, obj_name, **kwargs):
        obj_path = self.root_path.joinpath(obj_name)
        if not obj_path.exists():
            obj_path.mkdir(parents=True)

        new_id = str(self._get_next_id(obj_name))
        data_to_write = '\n'.join([f'{k}={v}' for k, v in kwargs.items()])

        with obj_path.joinpath(new_id).open(mode='w+') as f:
            f.write(data_to_write)

    def init_db(self):
        pass

    def update(self, obj_nae, id, title='', body=''):
        pass



def get_db():
    if 'db' not in g:
        g.db = FileDatabase(Path(os.getcwd()).joinpath('db_data'))
    return g.db


def close_db(e=None):
    g.pop('db', None)


def init_db():
    db = get_db()
    db.init_db()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
